package fr.eni.demoDAL.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Classe permettant le chargement des paramètres de connexion à la BDD par l'intermédiaire d'un fichier texte
 * avec extension .properties
 * ou un fichier XML
 * @author Enrick Enet
 */
public class Settings {
    private static Properties properties;

    //Création d'un bloc statique qui permet de charger en mémoire avant l'appel à la méthode getProperty()
    static {
        try {
            properties = new Properties();
            properties.load(Settings.class.getResourceAsStream("connexion.properties"));
        } catch (IOException e) {
            System.out.println("Cause : " + e.getCause());
        }
    }

    /**
     * Méthode qui permet d'obtenir une chaine de caractères correspondant à la clé renseignée en paramètre.
     * @param cle - Objet de type String
     * @return - La valeur.
     */
    public static String getProperty(String cle) {
        String valeur = properties.getProperty(cle);
        return valeur;
    }


}
