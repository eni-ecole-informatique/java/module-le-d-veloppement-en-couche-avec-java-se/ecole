package fr.eni.demoDAL.util;

import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import fr.eni.demoDAL.dal.DALException;
import java.sql.*;

/**
 * Classe DataBaseConnection gérant la connexion et la déconnexion à la BDD
 * @author Enrick Enet
 */
public class DataBaseConnection {

    //Déclaration des constantes
    private static String CONST_URL;
    private static String CONST_USER;
    private static String CONST_PASSWORD;

    /**
     * Méthode permettant d'obtenir une connection.
     * @return - un objet de type Connection.
     * @throws DALException - une exception de type DALException.
     */
    public static Connection seConnecter() throws DALException {
        Connection connexion = null;

        try {
            DriverManager.registerDriver(new SQLServerDriver());
            CONST_URL = Settings.getProperty("URL");
            CONST_USER = Settings.getProperty("USER");
            CONST_PASSWORD = Settings.getProperty("PASSWORD");


            connexion = DriverManager.getConnection(CONST_URL, CONST_USER, CONST_PASSWORD);
        } catch (SQLException e) {
            throw new DALException("Problème sur la chaine de connexion", e);
        }

        try {
            connexion.setAutoCommit(false);
        } catch (SQLException e){
            throw new DALException("Problème sur la désactivation de l'auto commit", e);
        }

        return connexion;
    }

    /**
     * Méthode permettant de fermer la connexion ouverte.
     * @param connexion - objet de type Connection.
     * @throws DALException - une exception de type DALException.
     */
    public static void seDeconnecter(Connection connexion) throws DALException {
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException e) {
                throw new DALException("Problème de fermeture de connexion", e);
            }
        }
    }

    /**
     * Méthode permettant de fermer la connexion ouverte et le Statement.
     * @param statement - Objet de type Statement
     * @param connexion - Objet de type Connection
     * @throws DALException
     */
    public static void seDeconnecter(Statement statement, Connection connexion) throws DALException {
        try {
            connexion.setAutoCommit(true);
        } catch (SQLException e){
            throw new DALException("Problème sur l'activation de l'auto commit", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new DALException("Problème de fermeture du statement", e);
            }
            seDeconnecter(connexion);
        }
    }




    /**
     * Méthode permettant de fermer la connexion ouverte et le preparedStatement
     * @param preparedStatement - objet de type PreparredStatement
     * @param connexion - objet de type Connection.
     * @throws DALException - une exception de type DALException.
     */
    public static void seDeconnecter(PreparedStatement preparedStatement, Connection connexion) throws DALException {
        try {
            connexion.setAutoCommit(true);
        } catch (SQLException e){
            throw new DALException("Problème sur l'activation de l'auto commit", e);
        } finally {
            try {
                if (preparedStatement != null){
                    preparedStatement.close();
                }
            } catch (SQLException e) {
                throw new DALException("Problème de fermeture du preparedStatement", e);
            }
            seDeconnecter(connexion);
        }
    }
}
