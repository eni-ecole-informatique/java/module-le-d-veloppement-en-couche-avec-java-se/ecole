package fr.eni.demoDAL.dal;

import fr.eni.demoDAL.bo.Eleve;
import fr.eni.demoDAL.dal.DALException;
import fr.eni.demoDAL.util.DataBaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Enrick Enet
 */
public class EleveDAOSqlServerImpl {
    //declaration des constantes
    private static final String LISTER = "{ call listerEleve }";
    private static final String RECHERCHER = "{ call rechercherEleve(?,?) }";
    private static final String INSERER = "{ call insererEleve(?,?,?) }";
    private static final String MODIFIER = "{ call modifierEleve(?,?,?) }";
    private static final String SUPPRIMER = "{ call supprimerEleve(?,?) }";

    private static final String COMPTER="{ ?=call compterEleve }";

    /**
     * Méthode lister() qui permet de lister tous les élèves présents dans la table Eleve de la BDD
     * @return - la liste des élèves (qui peut être vide mais pas null).
     * @throws DALException - Une exception de type DALException.
     * @finally - ferme les connexions ouvertes.
     */
    public static List<Eleve> lister() throws DALException {
        //Déclaration des variables, des instances et initialisation :
        Connection connexion = null;
        CallableStatement callableStatement = null; //On l'utilise lorsqu'il n'y a pas de paramètres dans la requête (requête non paramétrée).
        ResultSet resultSet = null; //On stocke le résultat (Résultat du SELECT).
        List<Eleve> eleves = new ArrayList<>(); //On instancie la liste, donc elle ne sera pas null.
        Eleve eleve = null;

        //******** 1ERE ETAPE ********//
        // Préparation de la connexion et gestion de l'exception //
        connexion = DataBaseConnection.seConnecter(); //Pas de try/catch ici car déjà traitée dans DALException.
        try {
            callableStatement = connexion.prepareCall(LISTER);
            resultSet = callableStatement.executeQuery();

            //******** 2EME ETAPE ********//
            // Récupération des données //
            while (resultSet.next()) {
                //Création d'un élève et chargement des attributs :
                eleve = new Eleve();
                eleve.setNom(resultSet.getString("nom"));  //On récupère le nom de l'élève par le nom de colonne.
                eleve.setPrenom(resultSet.getString("prenom")); //Idem pour le prénom.
                //Attention : l'adresse peut être null
                resultSet.getString("adresse"); // On positionne le curseur sur la cellule adresse.
                if (resultSet.wasNull()) {
                    eleve.setAdresse("<< Adresse inconnue >>");
                } else {
                    eleve.setAdresse(resultSet.getString("adresse"));
                }
                //On ajoute les élèves à la liste d'élèves.
                eleves.add(eleve);
            }

        } catch (SQLException e) {
            throw new DALException("Problème sur la méthode lister().", e);
        } finally {
            DataBaseConnection.seDeconnecter(callableStatement,connexion);
        }
        return eleves;
    }

    /**
     * Méthode rechercher() qui permet de rechercher un élève présent dans la table Eleve de la BDD
     * @return - l'élève recherché.
     * @throws DALException - Une exception de type DALException.
     * @finally - ferme les connexions ouvertes.
     */
    public static Eleve rechercher(String nom, String prenom) throws DALException {
        //Déclaration des variables, des instances et initialisation
        Connection connexion = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        Eleve eleve = new Eleve();

        connexion = DataBaseConnection.seConnecter();
        try {
            callableStatement = connexion.prepareCall(RECHERCHER);
            callableStatement.setString(1,nom);
            callableStatement.setString(2, prenom);
            resultSet = callableStatement.executeQuery();

            if (resultSet.next()) {
                eleve.setNom(resultSet.getString("nom"));
                eleve.setPrenom(resultSet.getString("prenom"));
                resultSet.getString("adresse"); // On positionne le curseur sur la cellule 'adresse'.
                if (resultSet.wasNull()) {
                    eleve.setAdresse("<< Adresse inconnue >>");
                } else {
                    eleve.setAdresse(resultSet.getString("adresse"));
                }
            }

        } catch (SQLException e) {
            throw new DALException("Problème sur la méthode rechercher()", e);
        } finally {
            DataBaseConnection.seDeconnecter(callableStatement,connexion);
        }
        return eleve;
    }

    /**
     * Méthode insérer() qui permet d'insérer un élève dans la table Eleve de la BDD
     * @return - le nombre de lignes insérées.
     * @throws DALException - Une exception de type DALException.
     * @finally - ferme les connexions ouvertes.
     */
    public static int inserer(Eleve eleve) throws DALException {
        Connection connexion = null;
        CallableStatement callableStatement = null;
        int nbLignes = 0;

        connexion = DataBaseConnection.seConnecter();

        try {
            callableStatement = connexion.prepareCall(INSERER);
            callableStatement.setString(1, eleve.getNom());
            callableStatement.setString(2, eleve.getPrenom());
            callableStatement.setString(3, eleve.getAdresse());
            nbLignes = callableStatement.executeUpdate();
            if (nbLignes == 1) {
                connexion.commit();
            } else {
                connexion.rollback();
            }

        } catch (SQLException e) {
            throw new DALException("Problème sur la méthode insérer()", e);
        } finally {
            DataBaseConnection.seDeconnecter(callableStatement, connexion);
        }
            return nbLignes;
    }

    /**
     * Méthode modifier() qui permet de modifier l'adresse
     * @param eleve - une instance d'Elève.
     * @throws DALException - Une exception de type DALException.
     * @finally - ferme les connexions ouvertes.
     */
    public static int modifier(Eleve eleve) throws DALException {
        Connection connexion = null;
        CallableStatement callableStatement = null;
        int nbLignes = 0;

        connexion = DataBaseConnection.seConnecter();

        try {
            callableStatement = connexion.prepareCall(MODIFIER);
            callableStatement.setString(1, eleve.getAdresse());
            callableStatement.setString(2, eleve.getNom());
            callableStatement.setString(3, eleve.getPrenom());
            nbLignes = callableStatement.executeUpdate();
            if (nbLignes == 1) {
                connexion.commit();
            } else {
                connexion.rollback();
            }

        } catch (SQLException e) {
            throw new DALException("Problème sur la méthode modifier()", e);
        } finally {
            DataBaseConnection.seDeconnecter(callableStatement, connexion);
        }
        return nbLignes;
    }

    /**
     * Méthode supprimer() qui permet de supprimer un élève de la BDD.
     * @param eleve - une instance d'Elève.
     * @return - le nombre de lignes à supprimer.
     * @throws DALException - Une exception de type DALException.
     * @finally - ferme les connexions ouvertes.
     */
    public static int supprimer(Eleve eleve) throws DALException {
        Connection connexion = null;
        CallableStatement callableStatement = null;
        int nbLignes = 0;

        connexion = DataBaseConnection.seConnecter();

        try {
            callableStatement = connexion.prepareCall(SUPPRIMER);
            callableStatement.setString(1, eleve.getNom());
            callableStatement.setString(2, eleve.getPrenom());
            nbLignes = callableStatement.executeUpdate();
            if (nbLignes == 1) {
                connexion.commit();
            } else {
                connexion.rollback();
            }

        } catch (SQLException e) {
            throw new DALException("Problème sur la méthode supprimer()", e);
        } finally {
            DataBaseConnection.seDeconnecter(callableStatement, connexion);
        }
        return nbLignes;
    }

    /**
     * Methode qui permet de compter le nombre d'�l�ves dans la BDD
     * @return le nombre d'eleves
     * @throws DALException - propage une exception de type DALException
     * @finally ferme les connexions ouvertes
     */
    public static int compter() throws DALException {
        Connection connexion = null;
        CallableStatement callableStatement = null;
        int nbEleves = 0;

        connexion = DataBaseConnection.seConnecter();
        try {
            callableStatement = connexion.prepareCall(COMPTER);
            callableStatement.registerOutParameter(1, java.sql.Types.INTEGER);
            callableStatement.execute();
            nbEleves = callableStatement.getInt(1);

        } catch (SQLException e) {
            throw new DALException("probleme sur la methode compter()", e);
        } finally {

            DataBaseConnection.seDeconnecter(callableStatement, connexion);
        }
        return nbEleves;
    }
}
