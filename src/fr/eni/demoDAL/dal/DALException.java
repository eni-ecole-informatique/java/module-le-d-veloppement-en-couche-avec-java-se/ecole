package fr.eni.demoDAL.dal;

public class DALException extends Exception {

    private static final long serialVersionUID = 1L;

    public DALException() {
    }

    public DALException(String message) {
        super(message);
    }

    public DALException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder("Couche DAL - ");
        sb.append(super.getMessage());
        return sb.toString();
    }
}
