package fr.eni.demoDAL.ihm;


import fr.eni.demoDAL.bo.Eleve;
import fr.eni.demoDAL.dal.DALException;
import fr.eni.demoDAL.dal.EleveDAOSqlServerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Enrick Enet
 */
public class TestDAL {
    public static void main(String[] args) {
        //Lister les élèves
        System.out.println("LISTE DES ELEVES");
        List<Eleve> eleves = new ArrayList<>();

        try {
            eleves = EleveDAOSqlServerImpl.lister();
            if (eleves.isEmpty()) {
                System.out.println("Aucun élève dans la Base de donnée.");
            } else {
                //Parcours du tableau
                for (Eleve eleve : eleves) {
                    System.out.println(eleve);
                }
            }

        //Rechercher un élève
            System.out.println("RECHERCHER UN ELEVE");
            Eleve eleveRecherche = new Eleve("Durand", "Sophie");
            eleveRecherche = EleveDAOSqlServerImpl.rechercher(eleveRecherche.getNom(), eleveRecherche.getPrenom());
            if (eleveRecherche.getNom() == null) {
                System.out.println("L'élève " + eleveRecherche + " n'existe pas.");
            } else {
                System.out.println(eleveRecherche);
            }

        //Insérer un élève
            System.out.println("INSERER UN ELEVE");
            Eleve eleveAjoute = new Eleve("NomTest", "PrenomTest");
            eleveAjoute.setAdresse("Adresse de test");
            int nbLigne = EleveDAOSqlServerImpl.inserer(eleveAjoute);
            if (nbLigne == 1) {
                System.out.println("Elève : " + eleveAjoute + " bien inséré(e) en BDD.");
            } else {
                System.out.println("Problème d'insertion en BDD de l'élève : \n" + eleveAjoute );
            }

        //Modifier l'adresse d'un élève
            System.out.println("MODIFIER L'ADRESSE D'UN ELEVE");
            Eleve eleveModifie = new Eleve("Dupont", "Jean");
            eleveAjoute.setAdresse("22B Baker Street - London");
            nbLigne = EleveDAOSqlServerImpl.modifier(eleveModifie);
            if (nbLigne == 1) {
                System.out.println("Elève : " + eleveModifie + " a bien été modifié(e).");
            } else {
                System.out.println("Problème sur la modification d'une adresse en BDD de l'élève : \n" + eleveModifie );
            }

        //Supprimer un élève
            System.out.println("SUPPRIMER UN ELEVE");
            Eleve eleveSupprime = new Eleve("NomTest", "PrenomTest");
            nbLigne = EleveDAOSqlServerImpl.supprimer(eleveSupprime);
            if (nbLigne == 1) {
                System.out.println("Elève : " + eleveSupprime + " a bien été supprimé(e).");
            } else {
                System.out.println("Problème sur la suppression en BDD de l'élève : \n" + eleveSupprime );
            }

        //Compter le nombre d'élèves
            int nbEleves = EleveDAOSqlServerImpl.compter();
            System.out.println("Nombre d'élève(s) dans la BDD: " + nbEleves);



        } catch (DALException e) {
            System.err.println("ERREUR \n Message : " + e.getMessage() + "- Cause : " + e.getCause());
            System.err.println("TRACE \n Classe : " + e.getStackTrace()[0].getClassName());
            System.err.println("Méthode : " + e.getStackTrace()[0].getMethodName());
            System.err.println("Ligne : " + e.getStackTrace()[0].getLineNumber());
        }


    }
}
