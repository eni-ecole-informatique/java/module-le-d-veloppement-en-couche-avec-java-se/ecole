package fr.eni.demoDAL.ihm;

import fr.eni.demoDAL.dal.DALException;
import fr.eni.demoDAL.util.DataBaseConnection;

import java.sql.Connection;

/**
 * Classe TestConnexion permettant de tester la connexion à la BDD.
 * @author Enrick Enet
 */
public class TestConnexion {

    public static void main(String[] args) {
        Connection connexion = null;
        //Si méthode non statique dans la classe DataBaseConnection
        //DataBaseConnection dbConnection = new DataBaseConnection();
        //dbConnection.seConnecter();

        try {
            DataBaseConnection.seConnecter();
            System.out.println("Connexion à la BDD - OK ");
        } catch (DALException e) {
            afficherErreur(e);
        } finally {
            try {
                DataBaseConnection.seDeconnecter(connexion);
            } catch (DALException e) {
                afficherErreur(e);
            }
        }
    }

    public static void afficherErreur(DALException e) {
        System.err.println("ERREUR \n Message : " + e.getMessage() + "- Cause : " + e.getCause());
        System.err.println("TRACE \n Classe : " + e.getStackTrace()[0].getClassName());
        System.err.println("Méthode : " + e.getStackTrace()[0].getMethodName());
        System.err.println("Ligne : " + e.getStackTrace()[0].getLineNumber());
    }
}
