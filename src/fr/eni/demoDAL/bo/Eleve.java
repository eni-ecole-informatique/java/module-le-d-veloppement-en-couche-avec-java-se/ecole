package fr.eni.demoDAL.bo;

/**
 * Classe POJO Eleve représentant le modèle métier.
 * Comportant les attributs :
 * Nom : le nom de l'élève.
 * Prénom : le prénom de l'élève.
 * L'adresse : l'adresse de l'élève.
 * @author Enrick Enet
 */
public class Eleve {
    //Attributs
    private String nom, prenom, adresse;

    //Constructeurs
    /**
     * Constructeur par défaut
     */
    public Eleve() {
        super();
    }

    /**
     * Constructeur surchargé qui prend en paramètres :
     * @param nom - Le nom de l'élève.
     * @param prenom - Le prénom de l'élève.
     */
    public Eleve(String nom, String prenom) {
        super();
        setNom(nom);
        setPrenom(prenom);
    }

    //Autres Méthodes
    /**
     * Méthode qui redéfinie la méthode  toString() de la classse Object
     * permettant d'afficher les attributs d'une instance d'Eleve.
     * @return - les attributs d'une instance d'Eleve
     */
    @Override
    public String toString() {
        return "Eleve [Nom=" + getNom() + '\'' +
                ", Prénom='" + getPrenom() + '\'' +
                ", Adresse='" + getAdresse() + '\'' + "]";
    }

    //Accesseurs et Mutateurs
    public String getNom() { return nom; }

    public void setNom(String nom) { this.nom = nom; }

    public String getPrenom() { return prenom; }

    public void setPrenom(String prenom) { this.prenom = prenom; }

    public String getAdresse() { return adresse; }

    public void setAdresse(String adresse) { this.adresse = adresse; }
}
